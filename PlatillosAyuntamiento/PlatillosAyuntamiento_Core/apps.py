from django.apps import AppConfig


class PlatillosayuntamientoCoreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'PlatillosAyuntamiento_Core'

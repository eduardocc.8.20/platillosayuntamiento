import uuid
import os
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User, BaseUserManager, AbstractBaseUser, PermissionsMixin
from .basemodels import BaseLog, BaseCrud

def platillo_imagen_path(instance, file_name):
    """Generate file path"""
    ext = file_name.split('.')[-1]
    file_name = f'{uuid.uuid4()}.{ext}'
    return os.path.join('imagenes/platillos/', file_name)

class Platillo(BaseCrud):
    precio = models.FloatField()
    activo = models.BooleanField(default=True)
    imagen = models.ImageField(null=True, upload_to=platillo_imagen_path, height_field=None, width_field=None, max_length=None)

    class Meta:
        db_table = 'platillos'

    def __str__(self):
        return self.nombre

pedido_status_choices = [
    (0, 'Pendiente'),
    (1, 'En Proceso'),
    (2, 'Completado'),
    (3, 'Cancelado'),
]
class Pedido(BaseLog):
    usuario_creador = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, on_delete=models.CASCADE)
    platillo = models.ForeignKey(Platillo, on_delete=models.CASCADE)
    estatus = models.PositiveIntegerField(default=0, choices=pedido_status_choices)

    class Meta:
        db_table = 'pedidos'

#Auth
class UserManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):        
        if not email:
            raise ValueError('Los usuarios deben tener un email.')
        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):        
        user = self.create_user(email, password)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):    
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    objects = UserManager()
    USERNAME_FIELD = 'email'



    
# Create your models here.

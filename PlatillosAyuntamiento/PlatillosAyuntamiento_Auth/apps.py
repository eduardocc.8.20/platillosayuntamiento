from django.apps import AppConfig


class PlatillosayuntamientoAuthConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'PlatillosAyuntamiento_Auth'

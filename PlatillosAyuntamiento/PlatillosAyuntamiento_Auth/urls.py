from django.urls import path
from PlatillosAyuntamiento_Auth import views


app_name = 'user'

urlpatterns = [
    path('crear/', views.CreateUserView.as_view(), name='crear'),
    path('token/', views.CreateTokenView.as_view(), name='token'),
    path('me/', views.ManageUserView.as_view(), name='me'),
]

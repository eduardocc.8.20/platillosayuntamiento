from django.urls import include, path
# from .views import CrearPlatilloView, ListaPlatilloView, ActualizarPlatilloView
from .views import PlatilloViewSet, PedidoViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('platillo', PlatilloViewSet)
router.register('pedido', PedidoViewSet)

app_name = 'api'

urlpatterns = [
    # path("crear/", CrearPlatilloView.as_view(), name="crear-platillo"),
    # path("actualizar/<int:pk>/", ActualizarPlatilloView.as_view(), name="actualizar-platillo"),
    # path("", ListaPlatilloView.as_view()),
    path('', include(router.urls))

]

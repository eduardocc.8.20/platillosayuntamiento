from django.db import models
from django.contrib.auth.models import User

class BaseLog(models.Model):
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    # created_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_related',
    #  related_query_name="%(app_label)s_%(class)ss")
    # modified_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_related',
    #  related_query_name="%(app_label)s_%(class)ss") 

    class Meta:
        abstract = True

class BaseCrud(BaseLog):
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=100)

    class Meta:
        abstract = True
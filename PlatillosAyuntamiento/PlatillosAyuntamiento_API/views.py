from django.shortcuts import render
from rest_framework import generics, authentication, permissions, viewsets, status
from rest_framework.response import Response
from PlatillosAyuntamiento_Core.models import Platillo, Pedido
from .serializers import PlatilloSerializer, PedidoSerializer
from datetime import datetime, timezone

#Mixins
class PermissionsMixin:
    def get_permissions(self):
        permissions_ = (permissions.DjangoModelPermissions(), permissions.IsAuthenticated(),)
        if self.action in ['list', 'retrieve']:
            permissions_ = []            
        return permissions_    

#Platillos
class PlatilloViewSet(PermissionsMixin, viewsets.ModelViewSet):
    queryset = Platillo.objects.all()
    serializer_class = PlatilloSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    #permission_classes = (permissions.DjangoModelPermissions, permissions.IsAuthenticated,)

    def get_queryset(self):                
        return self.queryset.filter(activo=True).order_by('-id')

# class ListaPlatilloView(viewsets.ModelViewSet):    
#     queryset = Platillo.objects.all()
#     serializer_class = PlatilloSerializer

# class ActualizarPlatilloView(viewsets.ModelViewSet):
#     queryset = Platillo.objects.all()
#     serializer_class = PlatilloSerializer

#Pedidos
class PedidoViewSet(viewsets.ModelViewSet):
    queryset = Pedido.objects.all()
    serializer_class = PedidoSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):       
        return self.queryset.filter(usuario_creador_id=self.request.user.id)

    def perform_create(self, serializer):
        serializer.save(usuario_creador=self.request.user);
        return super().perform_create(serializer)
            
        

# class ListaPedidoView(generics.ListAPIView):    
#     queryset = Pedido.objects.all()
#     serializer_class = PedidoSerializer

# class ActualizarPedidoView(generics.RetrieveUpdateAPIView):
#     queryset = Pedido.objects.all()
#     serializer_class = PedidoSerializer

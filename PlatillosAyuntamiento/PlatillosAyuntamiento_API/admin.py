from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseAdminUser
from django.utils.translation import gettext as _
from PlatillosAyuntamiento_Core import models, basemodels

class UserAdmin(BaseAdminUser):
    ordering = ['id']
    list_display = ['id', 'email', 'name']
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('name',)}),
        (
            _('Permissions'),
            {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups')}
        ),
        (_('Important dates'), {'fields': ('last_login',)}),
    )

class PedidoAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display = ['id', 'platillo', 'usuario_creador']
    

admin.site.register(models.User, UserAdmin)

admin.site.register(models.Platillo)
admin.site.register(models.Pedido, PedidoAdmin)


# Register your models here.

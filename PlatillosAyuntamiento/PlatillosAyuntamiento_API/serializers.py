from datetime import datetime, timezone
from rest_framework import serializers, viewsets, mixins
from PlatillosAyuntamiento_Core.models import Platillo, Pedido

class PlatilloSerializer(serializers.ModelSerializer):
    class Meta:
        model = Platillo
        fields = ['id', 'nombre', 'descripcion', 'precio', 'imagen', 'activo']

class PedidoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pedido
        fields = '__all__'
        # fields = ['id', 'estatus']        


    def validate(self, data):
        ultimo_pedido = Pedido.objects.filter(usuario_creador_id=self.context['request'].user.id).last()
        if ultimo_pedido:
            difdays = (datetime.now(timezone.utc) - ultimo_pedido.fecha_creacion).days
            if difdays <= 0:
                raise serializers.ValidationError("No es posible realizar más de un pedido el mismo día.")
        
        return data
                

from django.apps import AppConfig


class PlatillosayuntamientoApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'PlatillosAyuntamiento_API'

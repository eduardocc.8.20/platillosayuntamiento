from datetime import datetime, timezone
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from PlatillosAyuntamiento_API import views
from django.contrib.auth import get_user_model

from PlatillosAyuntamiento_Core.models import Platillo, Pedido

PEDIDO_URL = reverse('api:pedido-list')

def platillo_dummy():
    platillo = Platillo.objects.create(
            nombre='Huevos Revueltos',
            descripcion='3 huevos revueltos con frijol y salsa',
            precio=100,
            activo=True
        )
    return platillo

class PedidoApiTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user('nora@gmail.com', 'nora123')        

    def test_get_pedido_sin_auth(self):
        response = self.client.get(PEDIDO_URL)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)            

    def test_get_pedido__auth(self):
        #Autenticación   
        self.client.force_authenticate(self.user)

        response = self.client.get(PEDIDO_URL)
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)    

    def test_post_pedido_sin_auth(self):
        platillo = platillo_dummy()
        payload = {
            'platillo' : platillo.id
        }
        response = self.client.post(PEDIDO_URL, payload)        
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_post_pedido_auth(self):
        #Autenticación   
        self.client.force_authenticate(self.user)

        platillo = platillo_dummy()
        payload = {
            'platillo' : platillo.id
        }
        response = self.client.post(PEDIDO_URL, payload)    
                
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


    


# class YourTestClass(TestCase):

#     def setUp(self):
#         #Setup run before every test method.
#         pass

#     def tearDown(self):
#         #Clean up run after every test method.
#         pass

#     def test_something_that_will_pass(self):
#         self.assertFalse(True)

#     def test_something_that_will_fail(self):
#         self.assertTrue(False)
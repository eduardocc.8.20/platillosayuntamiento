from datetime import datetime, timezone
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from PlatillosAyuntamiento_API import views
from django.contrib.auth import get_user_model

from PlatillosAyuntamiento_Core.models import Platillo

PLATILLO_URL = reverse('api:platillo-list')

def platilloPayLoad(**params):
    payLoad = {
            'nombre': 'Huevos Revueltos',
            'descripcion': '1 huevo revueltos con frijol y salsa',
            'precio': 100,
            'activo': True
        }
    payLoad.update(params);
    return payLoad

class PlatilloApiTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user('nora@gmail.com', 'nora123')
        self.adminUser = get_user_model().objects.create_superuser('eduardocc.8.20@gmail.com', 'lalo123')
    
    def tearDown(self):
        pass

    def test_get_platillo_sin_auth(self):
        response = self.client.get(PLATILLO_URL)

        self.assertEqual(response.status_code, status.HTTP_200_OK)            

    def test_get_platillo__auth(self):
        response = self.client.get(PLATILLO_URL)

        self.assertEqual(response.status_code, status.HTTP_200_OK)    
    
    def test_get_retrieve_platillo_sin_auth(self):
        platillo = platilloPayLoad()
        platilloCreado = Platillo.objects.create(**platillo)

        response = self.client.get(PLATILLO_URL, {'id': platilloCreado.id})

        self.assertEqual(response.status_code, status.HTTP_200_OK)            

    def test_get_retrieve_platillo__auth(self):
        platillo = platilloPayLoad()        
        platilloCreado = Platillo.objects.create(**platillo)

        response = self.client.get(PLATILLO_URL, {'id': platilloCreado.id})

        self.assertEqual(response.status_code, status.HTTP_200_OK)       

    def test_post_platillo_sin_admin(self):
        payload = platilloPayLoad()

        response = self.client.post(PLATILLO_URL, payload)        
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_post_platillo_auth(self):
        #Autenticación   
        self.client.force_authenticate(self.user)

        payload = platilloPayLoad()
        response = self.client.post(PLATILLO_URL, payload)    
                
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_platillo_admin_auth(self):
        #Autenticación   
        self.client.force_authenticate(self.adminUser)
        payload = platilloPayLoad()
        response = self.client.post(PLATILLO_URL, payload)    
                
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


    


# class YourTestClass(TestCase):

#     def setUp(self):
#         #Setup run before every test method.
#         pass

#     def tearDown(self):
#         #Clean up run after every test method.
#         pass

#     def test_something_that_will_pass(self):
#         self.assertFalse(True)

#     def test_something_that_will_fail(self):
#         self.assertTrue(False)
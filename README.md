# PlatillosAyuntamiento

Desarrollo prueba en Django Rest Framework

## Windows

### Crear entorno virtual:

```
py -m venv PlatillosAyuntamiento.Env
```

### Activar entorno

```
.\PlatillosAyuntamiento.Env\Scripts\Activate.ps1
```

### Instalar Requirements

```
pip install -r requirements.txt
```

### Levantar docker-compose

```
docker-compose up
```

### Crear superuser

```
docker-compose run -rm api sh -c "python manage.py createsuperuser"
```

### Testing

```
docker-compose run —rm api sh -c “python manage.py test”
```
